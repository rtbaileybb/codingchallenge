﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1Tester
{
    class Program
    {
        static HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            client.BaseAddress = new Uri("https://localhost:44369/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var creds = "coding:challenge";
            var encoded = Convert.ToBase64String(Encoding.Default.GetBytes(creds));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Authorization", encoded);

            //var newEmp1 = new Employee { Department = "123", FullName = "Bud Abbott", PhoneNumber = "8005551212" };
            //AddEmployee(newEmp1).Wait();
            //var newEmp2 = new Employee { Department = "234", FullName = "Lou Costello", PhoneNumber = "8005552424" };
            //AddEmployee(newEmp2).Wait();
            //var newEmp3 = new Employee { Department = "123", FullName = "Huey Duck", PhoneNumber = "8005553636" };
            //AddEmployee(newEmp3).Wait();
            //var newEmp4 = new Employee { Department = "234", FullName = "Dewey Duck", PhoneNumber = "8005554848" };
            //AddEmployee(newEmp4).Wait();
            //var newEmp5 = new Employee { Department = "123", FullName = "Louie Duck", PhoneNumber = "8005555050" };
            //AddEmployee(newEmp5).Wait();

            TestGetEmployees().Wait();

            //TestCrud().Wait();

            Console.ReadLine();
        }

        public static async Task TestGetEmployees()
        {
            var empList = await GetEmployees();
            Console.WriteLine("found {0} employees", empList.Count);
            foreach (var emp in empList)
            {
                Console.WriteLine("\t{0}\t{1}", emp.Id, emp.FullName);
            }
        }

        public static async Task TestCrud()
        {
            //add
            var newEmp10 = new Employee { Department = "test", FullName = "Robert Bailey", PhoneNumber = "1234567" };
            AddEmployee(newEmp10).Wait();

            var empList = await GetEmployees();
            Console.WriteLine("found {0} employees (initial)", empList.Count);
            var testEmp10 = empList.FirstOrDefault(p => p.FullName.Equals("Robert Bailey"));
            if (testEmp10 != null)
            {
                //get by ID
                var testEmp11 = await GetEmployee(testEmp10.Id);
                Console.WriteLine("Test: {0}, against {1} (they should be the same)", testEmp10.FullName, testEmp11.FullName);

                //update
                testEmp10.Department = "new dept";
                UpdateEmployee(testEmp10.Id, testEmp10).Wait();
                var testEmp12 = await GetEmployee(testEmp10.Id);
                Console.WriteLine("Test: {0}, against {1} (they should be the same)", testEmp10.Department, testEmp12.Department);

                //delete
                DeleteEmployee(testEmp10.Id).Wait();
                var updatedList = await GetEmployees();
                Console.WriteLine("found {0} employees (after testing)", updatedList.Count);
            }

        }

        public static async Task<List<Employee>> GetEmployees()
        {
            List<Employee> result = null;
            HttpResponseMessage response = await client.GetAsync("api/employee");
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<List<Employee>>();
            }
            return result;
        }

        public static async Task<Employee> GetEmployee(long id)
        {
            Employee result = null;
            HttpResponseMessage response = await client.GetAsync($"api/employee/{id}");
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<Employee>();
            }
            return result;
        }

        public static async Task AddEmployee(Employee model)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync("api/employee", model);
            response.EnsureSuccessStatusCode();
        }

        public static async Task UpdateEmployee(long id, Employee model)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync($"api/employee/{id}", model);
            response.EnsureSuccessStatusCode();
        }

        public static async Task DeleteEmployee(long id)
        {
            HttpResponseMessage response = await client.DeleteAsync($"api/employee/{id}");
            response.EnsureSuccessStatusCode();
        }
    }

    class Employee
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Department { get; set; }
    }
}
