I understand fully that this coding challenge is not due for another 2 days.
But I work full time, I'm in grad school, and I have a large family, so this is my best work given my own time constraints.
I also believe that getting things done quickly and early is the best approach. I hope you do too.
And I also want you to have more time to peruse my code without feeling rushed.

If you have any questions, feel free to reach out to me at baileyrt@gmail.com