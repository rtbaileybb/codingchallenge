This is the one I'm most "worried" about, because local EF is notorious for its "works on my machine" certification.
It does work on my machine, and I believe it will work on any machine, but it might take some setup.

It's a Console application, using EF code-first.
I tried to add comments and give attribution, embedded in the code where it was applicable.

I wanted a really large data set, so I set it up with 200 Departments and 10,000 Employees.
But in order to do that, I had to generate random names, so they're not really readable.