﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EntityFrameworkConsole
{
    //more info about eager and lazy loading here: https://docs.microsoft.com/en-us/ef/ef6/querying/related-data

    class Program
    {
        private static readonly char[] UpperLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        private static readonly char[] LowerLetters = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
        private static readonly char[] Digits = "1234567890".ToCharArray();

        static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            long eagerTotal = 0;
            long lazyTotal = 0;
            string validDept = "";

            //if this is your first time running this example and if there's no data in the local DB,
            //   set this to true, run it once, and then come back and set it to false and run it again
            var initialSetup = false;
            if (initialSetup)
                LoadTables();
            else
            {
                using (var context = new EmployeeContext(true))
                {
                    var firstDept = context.Departments.OrderBy(p => p.Id).Skip(21).First();
                    validDept = firstDept.Name;
                }
                Console.WriteLine("Starting with a valid dept: {0}", validDept);
                Console.WriteLine();

                //eager loading
                Console.WriteLine("Querying using eager loading first");
                using (var eagerCtx = new EmployeeContext(false))
                {
                    stopwatch.Start();
                    //the "Include" pulls in the related records
                    var result = eagerCtx.Departments.Include("Employees").FirstOrDefault(p => p.Name.Equals(validDept));
                    stopwatch.Stop();
                    if (result != null)
                        Console.WriteLine("Found [{0}] dept with {1} employees in {2} ticks",
                             result.Name, result.Employees.Count(), stopwatch.ElapsedTicks);
                    eagerTotal += stopwatch.ElapsedTicks;
                }
                stopwatch.Reset();
                Console.WriteLine();

                //lazy loading
                Console.WriteLine("Querying using lazy loading second");
                using (var lazyCtx = new EmployeeContext(true))
                {
                    stopwatch.Start();
                    var result = lazyCtx.Departments.FirstOrDefault(p => p.Name.Equals(validDept));
                    if (result != null)
                    {
                        Console.Write("Found [{0}] dept ", result.Name);
                        //here's where the lazy load kicks in
                        var empList = result.Employees.ToList();
                        stopwatch.Stop();
                        Console.Write("with {0} employees ", empList.Count());
                        Console.WriteLine("in {0} ticks", stopwatch.ElapsedTicks);
                        lazyTotal += stopwatch.ElapsedTicks;
                    }
                }

                Console.WriteLine();
                Console.WriteLine("Total time elapsed for eager loading: {0}", eagerTotal);
                Console.WriteLine("Total time elapsed for lazy loading:  {0}", lazyTotal);
            }
            Console.WriteLine("processing complete");
            Console.ReadLine();
        }

        static void LoadTables()
        {
            //the general idea of how to start with code-first and fill the tables came from here: 
            //  https://www.entityframeworktutorial.net/code-first/simple-code-first-example.aspx
            using (var context = new EmployeeContext(false))
            {
                //add 200 random departments
                CreateDepartments(context);
                Console.WriteLine("Created departments");

                //add 10,000 employees, distributed in those departments
                CreateEmployees(context);
                Console.WriteLine("Created employees");

                context.SaveChanges();
            }
        }
        static void CreateEmployees(EmployeeContext context)
        {
            int deptCount = context.Departments.Count();
            var deptList = context.Departments.ToList();
            var rand = new Random(DateTime.Now.Millisecond);

            for (int i = 0; i < 10000; i++)
            {
                var firstName = GenerateName();
                //select a random department and phone number, to pass some time
                var randomDept = deptList.ElementAt(rand.Next(0, deptCount - 1));
                var phonenum = GenerateNumber();
                //belt and suspenders
                Thread.Sleep(1);
                var lastName = GenerateName();
                var fullName = string.Format("{0} {1}", firstName, lastName);
                var newEmp = new Employee { Department = randomDept, FullName = fullName, Id = DateTime.Now.Ticks, PhoneNumber = phonenum };
                context.Employees.Add(newEmp);
            }
        }
        static void CreateDepartments(EmployeeContext context)
        {
            var rand = new Random(DateTime.Now.Millisecond);
            //create 200 new depts
            for (int i = 0; i < 200; i++)
            {
                var deptName = string.Format("{0} - {1}", rand.Next(100, 999), GenerateName());
                var newDept = new Department { Id = DateTime.Now.Ticks, Name = deptName };
                context.Departments.Add(newDept);
                Thread.Sleep(1);
            }
        }
        static string GenerateName()
        {
            var sb = new StringBuilder();
            var rand = new Random(DateTime.Now.Millisecond);
            //number of lowercase chars, after the first
            var stringLen = rand.Next(2, 9);
            //the first letter (uppercase)
            sb.Append(UpperLetters[rand.Next(0, UpperLetters.Length - 1)]);
            for (int i = 0; i < stringLen; i++)
            {
                sb.Append(LowerLetters[rand.Next(0, LowerLetters.Length - 1)]);
            }
            return sb.ToString();
        }
        static string GenerateNumber()
        {
            var sb = new StringBuilder();
            var rand = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < 10; i++)
            {
                sb.Append(Digits[rand.Next(0, Digits.Length - 1)]);
            }
            return sb.ToString();
        }
    }
}
