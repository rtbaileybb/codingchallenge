﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkConsole
{
    class EmployeeContext : DbContext
    {
        public EmployeeContext() : base() { }

        public EmployeeContext(bool isLazy)
        {
            this.Configuration.LazyLoadingEnabled = isLazy;
            this.Configuration.ProxyCreationEnabled = isLazy;
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
    }
}
