After opening this project, be sure to launch/debug the API first. That will start the VS web server.

You'll need to take the base URL of the API and paste it here:
CodingChallenge1\CodingChallenge.Web\Models\ServiceManager.cs
Replacing what's in quotes on this line of code:
ApiClient.BaseAddress = new Uri("https://localhost:44369/");

Then you can open the Web project and launch/debug it, to see it work.

I tried to add comments to anything I felt was tricky, and I tried to give attribution where it was used.

Up a level is a Challenge1Tester console app that I used to test the API, before wiring it into the MVC site.
It's not part of the project, but you're welcome to check it out.