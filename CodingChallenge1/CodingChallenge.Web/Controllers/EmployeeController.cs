﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Challenge.Shared;
using CodingChallenge.Web.Models;

namespace CodingChallenge.Web.Controllers
{
    //NOTE: this controller and all of the views were basically built from scaffolding
    public class EmployeeController : Controller
    {
        //not exactly Singleton pattern, but it'll do in a pinch
        public ServiceManager SvcMgr = new ServiceManager();

        // GET: Employee
        public async Task<ActionResult> Index()
        {
            List<Employee> empList = null;
            try
            {
                empList = await SvcMgr.GetEmployees();
            }catch(Exception ex)
            {
                Response.Write(ex);
            }

            return View(empList);
        }

        // GET: Employee/Details/5
        public async Task<ActionResult> Details(long id)
        {
            var deets = await SvcMgr.GetEmployee(id);
            return View(deets);
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            try
            {
                SvcMgr.AddEmployee(employee).Wait();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Edit/5
        public async Task<ActionResult> Edit(long id)
        {
            var deets = await SvcMgr.GetEmployee(id);
            return View(deets);
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(long id, Employee updated)
        {
            try
            {
                SvcMgr.UpdateEmployee(id, updated).Wait();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public async Task<ActionResult> Delete(long id)
        {
            var deets = await SvcMgr.GetEmployee(id);
            return View(deets);
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(long id, Employee deleted)
        {
            try
            {
                SvcMgr.DeleteEmployee(id).Wait();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
