﻿using Challenge.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CodingChallenge.Web.Models
{
    //much of this was adapted from: https://docs.microsoft.com/en-us/aspnet/web-api/overview/advanced/calling-a-web-api-from-a-net-client
    public class ServiceManager
    {
        static HttpClient ApiClient = new HttpClient();

        public ServiceManager()
        {
            if (ApiClient.BaseAddress == null)
            {
                ApiClient.BaseAddress = new Uri("https://localhost:44369/");
                ApiClient.DefaultRequestHeaders.Accept.Clear();
                ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //for basic auth
                var creds = "coding:challenge";
                var encoded = Convert.ToBase64String(Encoding.Default.GetBytes(creds));
                ApiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Authorization", encoded);
            }
        }

        public async Task<List<Employee>> GetEmployees()
        {
            List<Employee> result = null;
            HttpResponseMessage response = await ApiClient.GetAsync("api/employee").ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<List<Employee>>();
            }
            return result;
        }

        public async Task<Employee> GetEmployee(long id)
        {
            Employee result = null;
            HttpResponseMessage response = await ApiClient.GetAsync($"api/employee/{id}").ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<Employee>();
            }
            return result;
        }

        public async Task AddEmployee(Employee model)
        {
            HttpResponseMessage response = await ApiClient.PostAsJsonAsync("api/employee", model).ConfigureAwait(false);
            response.EnsureSuccessStatusCode();
        }

        public async Task UpdateEmployee(long id, Employee model)
        {
            HttpResponseMessage response = await ApiClient.PutAsJsonAsync($"api/employee/{id}", model).ConfigureAwait(false);
            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteEmployee(long id)
        {
            HttpResponseMessage response = await ApiClient.DeleteAsync($"api/employee/{id}").ConfigureAwait(false);
            response.EnsureSuccessStatusCode();
        }
    }
}