﻿using Challenge.Shared;
using CodingChallenge.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodingChallenge.API.Controllers
{
    //those was mostly built from scaffolding (pointing at Employee class)
    [BasicAuthentication]
    public class EmployeeController : ApiController
    {
        // GET: api/Employee

        public IEnumerable<Employee> Get()
        {
            return DataProvider.GetEmployees();
        }

        // GET: api/Employee/5
        public Employee Get(long id)
        {
            return DataProvider.GetEmployee(id);
        }

        // POST: api/Employee
        public void Post([FromBody]Employee value)
        {
            DataProvider.AddEmployee(value);
        }

        // PUT: api/Employee/5
        public void Put(long id, [FromBody]Employee value)
        {
            DataProvider.UpdateEmployee(id, value);
        }

        // DELETE: api/Employee/5
        public void Delete(long id)
        {
            DataProvider.DeleteEmployee(id);
        }
    }
}
