﻿using Challenge.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CodingChallenge.API.Models
{
    public static class DataProvider
    {
        //NOTE: for simplicity's sake, I'm storing the objects in a text file on the server
        // in real life, they'd be in a DB. I understand fully that file storage is not optimal
        // for web- especially with multiple web servers
        internal static List<Employee> GetEmployees()
        {
            try
            {
                //read the json string, convert it to objects
                var filePath = GetStoredListPath();
                var fileText = File.ReadAllText(filePath);
                var list = JsonConvert.DeserializeObject<List<Employee>>(fileText);
                return list;
            }
            catch (Exception) { return new List<Employee>(); }
        }

        internal static Employee GetEmployee(long id)
        {
            try
            {
                var storedList = GetEmployees();
                //first or default, just in case (belt and suspenders)
                var toBeUpdated = storedList.FirstOrDefault(p => p.Id.Equals(id));
                return toBeUpdated;
            }
            catch (Exception) { return null; }
        }

        internal static void DeleteEmployee(long id)
        {
            try
            {
                var storedList = GetEmployees();
                //could call GetEmployee(id), but that's getting the list twice
                var original = storedList.SingleOrDefault(p => p.Id.Equals(id));
                storedList.Remove(original);
                CommitList(storedList);
            }
            catch (Exception) { throw; }
        }

        internal static void AddEmployee(Employee model)
        {
            try
            {
                //ID is set only here
                model.Id = DateTime.Now.Ticks;
                var storedList = GetEmployees();
                storedList.Add(model);
                CommitList(storedList);
            }
            catch (Exception) { throw; }
        }

        internal static void UpdateEmployee(long id, Employee model)
        {
            try
            {
                var storedList = GetEmployees();
                var original = storedList.SingleOrDefault(p => p.Id.Equals(id));
                //could call DeleteEmployee, but that's getting the list twice
                storedList.Remove(original);
                storedList.Add(model);
                CommitList(storedList);
            }
            catch (Exception) { throw; }
        }

        private static void CommitList(List<Employee> list)
        {
            var attempts = 0;
            var terminate = false;
            do
            {
                try
                {
                    //serialize the list of objects, write it to the text file
                    var path = GetStoredListPath();
                    var serialized = JsonConvert.SerializeObject(list);
                    using (var file = new StreamWriter(path, false))
                        file.WriteLine(serialized);
                    terminate = true;
                }
                catch (Exception)
                {
                    attempts++;
                    if (attempts > 2)
                    {
                        terminate = true;
                        //TODO: tell someone about the exception
                    }
                }
            } while (!terminate);
        }

        private static string GetStoredListPath()
        {
            //the full path to the App_Data folder
            var path = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
            //normally, this would be stored in the config file or in the DB,
            //  but the objects would also be in the DB, so this will work
            var fileName = "EmployeeList.json";
            var filePath = string.Format("{0}\\{1}", path, fileName);
            return filePath;
        }
    }
}