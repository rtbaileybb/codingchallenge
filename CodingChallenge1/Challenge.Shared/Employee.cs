﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Shared
{
    public class Employee
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Department { get; set; }
    }
}
